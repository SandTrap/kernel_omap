#include <asm/sandtrap.h>
#include <asm/mmu_context.h>
#include <asm/pgalloc.h>
#include <asm/pgtable.h>
#include <asm-generic/mman-common.h>
#include <linux/syscalls.h>

#ifdef CONFIG_SANDTRAP_PROTECTION_PGTABLES
long sandtrap_setprotmode_pgtables(int mode)
{
	struct thread_info *thread = current_thread_info();
	struct task_struct *tsk = thread->task;
	struct mm_struct *mm = tsk->mm;

	down_write(&mm->mmap_sem);

	if (unlikely(!mm->pgd_protected))
		if (!pgd_protected_alloc(mm))
			goto outofmem;

	if (mode == IGNORE_SANDTRAP_PROTECTIONS) {
		if (tsk->active_pgd == mm->pgd_unprotected)
			goto nochange;

		tsk->active_pgd = mm->pgd_unprotected;
		goto switched;
	} else if (mode == OBEY_SANDTRAP_PROTECTIONS) {
		if (tsk->active_pgd == mm->pgd_protected)
			goto nochange;

		tsk->active_pgd = mm->pgd_protected;
		goto switched;
	}

	up_write(&mm->mmap_sem);
	return SANDTRAP_INVALID_REQUESTED_MODE;

nochange:
	up_write(&mm->mmap_sem);
	return SANDTRAP_NO_CHANGE;

outofmem:
	up_write(&mm->mmap_sem);
	return SANDTRAP_PROTECTION_OUTOFMEM;

switched:
	tsk->pgd_switched = true;
	switch_mm(tsk->mm, tsk->mm, tsk);
	up_write(&mm->mmap_sem);
	return mode;
}

long sandtrap_setmemprot_pgtables(unsigned long addr, int prot)
{
	unsigned int index;
	pmd_t *pmd_unprotected, *pmd_protected;
	pte_t *pte_unprotected, *pte_protected;

	pgtable_t new_pgtable = NULL;
	spinlock_t *ptl = NULL;
	struct vm_area_struct *vma;

	struct thread_info *thread = current_thread_info();
	struct task_struct *task = thread->task;
	struct mm_struct *mm = task->mm;

	down_write(&mm->mmap_sem);

	// Allocate the protected page table.
	if (unlikely(!mm->pgd_protected))
		if (!pgd_protected_alloc(mm))
			goto outofmem;

	// Check if the address to protect is valid.
	if (addr >= TASK_SIZE)
		goto invalidaddr;

	addr &= PAGE_MASK;
	vma = find_extend_vma(mm, addr);
	if (!vma || addr < vma->vm_start)
		goto invalidaddr;

	// Get a reference to the entry in the first level unprotected
	// table that corresponds to the address to protect.
	index = pgd_index(addr);
	pmd_unprotected = (pmd_t*)(mm->pgd_unprotected + index);

	// If the entry does not point to a second level page table,
	// allocate a new second level page table and populate the
	// first level unprotected table..
	if (unlikely(pmd_none(*pmd_unprotected)) && __pte_alloc(mm, vma, pmd_unprotected, addr))
	 	goto outofmem;

	// Get references to the second level tables in both the
	// unprotected and protected tables.
	pte_unprotected = pte_offset_map(pmd_unprotected, addr);
	pmd_protected = (pmd_t*)(mm->pgd_protected + index);
	pte_protected = pte_offset_map(pmd_protected, addr);

	// Allocate a separate second level page table for the
	// protected table in the event both the unprotected and
	// protected tables are pointing to the same second level
	// table. A separate table is required to hold a different set
	// of protection bits.
	if (unlikely(pte_unprotected == pte_protected)) {
		new_pgtable = pte_alloc_one(mm, addr);
		if (!new_pgtable)
		 	goto outofmem;

		// Copy everything from the unprotected second level
		// page table to the newly allocated page table.
		memcpy((void*)phys_to_virt(page_to_phys(new_pgtable)), __pte_map(pmd_unprotected), PTE_TABLE_SIZE);
		smp_wmb();

		spin_lock(&mm->page_table_lock);
		pte_unprotected = pte_offset_map(pmd_unprotected, addr);
		pte_protected = pte_offset_map(pmd_protected, addr);

		if (likely(pte_unprotected == pte_protected)) {
			pmd_populate_nosync(mm, pmd_protected, new_pgtable);
			new_pgtable = NULL;
		}
		spin_unlock(&mm->page_table_lock);

		if (new_pgtable)
			 pte_free(mm, new_pgtable);
	}

	pte_protected = pte_offset_map(pmd_protected, addr);
	pte_unprotected = pte_offset_map_lock(mm, pmd_unprotected, addr, &ptl);

	if (prot == DISABLE_SANDTRAP_PROTECTION)
		pte_val(*pte_protected) = pte_val(*pte_unprotected);
	else if (prot == ENABLE_SANDTRAP_PROTECTION)
		pte_val(*pte_protected) = pte_mksandtrap(*pte_unprotected);
	else
		goto invalidprotection;

	set_pte_at_nosync(mm, addr, pte_protected, pte_val(*pte_protected));
	flush_tlb_range(vma, addr, addr + PAGE_SIZE);
	goto success;

invalidaddr:
	up_write(&mm->mmap_sem);
	return SANDTRAP_INVALID_ADDRESS;

invalidprotection:
	pte_unmap_unlock(pte_unprotected, ptl);
	up_write(&mm->mmap_sem);
	return SANDTRAP_INVALID_REQUESTED_PROTECTION;

outofmem:
	up_write(&mm->mmap_sem);
	return SANDTRAP_PROTECTION_OUTOFMEM;

success:
	pte_unmap_unlock(pte_unprotected, ptl);
	up_write(&mm->mmap_sem);
	return prot;
}
#endif

#ifdef CONFIG_SANDTRAP_PROTECTION_DOMAINS
long sandtrap_setprotmode_domains(int mode)
{
	unsigned int curr_perms = 0;
	unsigned int new_perms = 0;
	struct thread_info *thread = current_thread_info();

	curr_perms = get_sandtrap_domain_permission(thread->cpu_domain);

	if (mode == IGNORE_SANDTRAP_PROTECTIONS) {
		new_perms = DOMAIN_MANAGER_UNCOND;
		if (curr_perms == new_perms)
			goto nochange;
		else
			goto success;
	} else if (mode == OBEY_SANDTRAP_PROTECTIONS) {
		new_perms = DOMAIN_CLIENT;
		if (curr_perms == new_perms)
			goto nochange;
		else
			goto success;
	}

	return SANDTRAP_INVALID_REQUESTED_MODE;

nochange:
	return SANDTRAP_NO_CHANGE;

success:
	modify_domain_uncond(DOMAIN_SANDTRAP, new_perms);
	return mode;
}

long sandtrap_setmemprot_domains(unsigned long addr, int prot)
{
	pmd_t *pmd;

	int i;
	int curr_domain;
	int prot_type;
	unsigned int index;
	unsigned long second_level_addr;

	struct vm_area_struct *vma;
	struct thread_info *thread = current_thread_info();
	struct task_struct *tsk = thread->task;
	struct mm_struct *mm = tsk->mm;

	down_write(&mm->mmap_sem);

	if (addr >= TASK_SIZE)
		goto invalidaddr;

	vma = find_extend_vma(mm, addr);
	if (!vma || addr < vma->vm_start)
		goto invalidaddr;

	index = pgd_index(addr);
	pmd = (pmd_t*)(mm->pgd + index);

	if (unlikely(pmd_none(*pmd)) && __pte_alloc(mm, vma, pmd, addr))
		goto outofmem;

	if (prot == DISABLE_SANDTRAP_PROTECTION) {
		// When disabling SandTrap protection, just restore
		// access permissions to the page. There is no need to
		// change the domain in the first-level page table.
		// TODO(ali): When restoring protections, restore it
		// whatever permissions the page had previously
		// instead of using a catch-all approach such as
		// PROT_READ | PROT_WRITE.
		prot_type = PROT_READ | PROT_WRITE;
		goto success;
	} else if (prot == ENABLE_SANDTRAP_PROTECTION) {

		// First, change the domain of the first-level page
		// table to DOMAIN_SANDTRAP.
		index = get_pmd_index(addr);
		curr_domain = get_pmd_domain(pmd[index]);
		if (curr_domain != DOMAIN_SANDTRAP) {
			pmd[index] = (pmd[index] & ~PMD_DOMAIN_MASK) + (DOMAIN_SANDTRAP << DOMAIN_BIT_SHIFT);
			flush_pmd_entry(pmd);
		}

		// Go through the second-level page table entries and
		// break CoW for all CoW pages.
		second_level_addr = (addr & SECTION_MASK);
		for (i = 0; i < NUM_PTES; i++) {
			vma = find_extend_vma(mm, second_level_addr);
			if (!vma || second_level_addr < vma->vm_start)
				goto next;

			// Only break CoW if the vma is a CoW
			// mapping. See mm/memory.c#is_cow_mapping.
			// TODO(ali): Do not call fixup_user_fault if
			// CoW has already been broken for it.
			if (((vma->vm_flags & (VM_SHARED | VM_MAYWRITE)) == VM_MAYWRITE))
				fixup_user_fault(tsk, mm, second_level_addr, FAULT_FLAG_WRITE);
next:
			second_level_addr += (1 << PTE_SHIFT);
		}

		prot_type = PROT_NONE;
		goto success;
	}

	up_write(&mm->mmap_sem);
	return SANDTRAP_INVALID_REQUESTED_PROTECTION;

invalidaddr:
	up_write(&mm->mmap_sem);
	return SANDTRAP_INVALID_ADDRESS;

outofmem:
	up_write(&mm->mmap_sem);
	return SANDTRAP_PROTECTION_OUTOFMEM;

success:
	up_write(&mm->mmap_sem);
	sys_mprotect(addr, PAGE_SIZE, prot_type);
	return prot;
}
#endif
