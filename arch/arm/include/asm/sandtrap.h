/*
 * arm/arm/include/asm/sandtrap.h
 *
 * Copyright (C) 2017 Ali Razeen
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 *  This file contains SandTrap specific declarations.
 */
#ifndef __ASM_ARM_SANDTRAP_H
#define __ASM_ARM_SANDTRAP_H

#define IGNORE_SANDTRAP_PROTECTIONS	1
#define OBEY_SANDTRAP_PROTECTIONS	2
#define ENABLE_SANDTRAP_PROTECTION	1
#define DISABLE_SANDTRAP_PROTECTION	2

#ifdef CONFIG_SANDTRAP_PROTECTION_PGTABLES
extern long sandtrap_setprotmode_pgtables(int mode);
extern long sandtrap_setmemprot_pgtables(unsigned long addr, int prot);
#endif

#ifdef CONFIG_SANDTRAP_PROTECTION_DOMAINS
extern long sandtrap_setprotmode_domains(int mode);
extern long sandtrap_setmemprot_domains(unsigned long addr, int prot);
#endif

#endif
